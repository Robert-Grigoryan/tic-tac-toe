class TicTacToe {
    
    constructor() {

        this.matrix = [];

        this.turns = 0;

        for ( var i = 0; i < 3; i++ ) {

            this.matrix[i] = [];

            for ( var j = 0; j < 3; j++ ) {

                this.matrix[i][j] = null;

                this.turns++;

            }

        }

        this.player = 'x';

        this.winner = null;

    }

    getCurrentPlayerSymbol() {

        return this.player;

    }

    nextTurn(rowIndex, columnIndex) {

        if ( this.matrix[rowIndex][columnIndex] === null ) {

            this.matrix[rowIndex].splice(columnIndex, 1, this.player);

            this.player === 'x' ? this.player = 'o': this.player = 'x';

            this.turns--;

        }

    }

    isFinished() {

        for ( var i = 0; i < this.matrix.length; i++ ) {

            if ( this.matrix[i][0] !== null
                && this.matrix[i][0] === this.matrix[i][1]
                && this.matrix[i][0] === this.matrix[i][2]
                && this.matrix[i][1] === this.matrix[i][2] ) {

                this.winner = this.matrix[i][0];

                return true;

            } else if ( this.matrix[0][i] !== null
                && this.matrix[0][i] === this.matrix[1][i]
                && this.matrix[0][i] === this.matrix[2][i]
                && this.matrix[1][i] === this.matrix[2][i] ) {

                this.winner = this.matrix[0][i];

                return true;

            }
        }

        if ( this.matrix[1][1] !== null 
            && this.matrix[0][0] === this.matrix[1][1] 
            && this.matrix[1][1] === this.matrix[2][2]
            && this.matrix[0][0] === this.matrix[2][2] ) {

            this.winner = this.matrix[1][1];

            return true;

        }

        if ( this.matrix[1][1] !== null 
            && this.matrix[2][0] === this.matrix[1][1]
            && this.matrix[1][1] === this.matrix[0][2]
            && this.matrix[2][0] === this.matrix[0][2] ) {

            this.winner = this.matrix[1][1];

            return true;

        }

        return this.noMoreTurns();
        
    }

    getWinner() {

        this.isFinished();

        return this.winner;
        
    }

    noMoreTurns() {

        return this.turns === 0 ? true: false;

    }

    isDraw() {

        if ( this.noMoreTurns() && !this.getWinner() ) {

            return true;

        } else {

            return false;

        }
        
    }

    getFieldValue(rowIndex, colIndex) {

        return this.matrix[rowIndex][colIndex];

    }

}

module.exports = TicTacToe;
